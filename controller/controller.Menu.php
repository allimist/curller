<?php

function controller_menu(){

	$menu = array(
		
		'caradd' => array(
			'url'  => '?action=caradd',
			'text' => 'Insert Car'
		),
		'carlist' => array(
			'url'  => '?action=carlist',
			'text' => 'Cars List'
		),

	);
	
	$view='Menu';
	include APP_REAL_PATH."/view/view.$view.php";	

}