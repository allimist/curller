<?php

function controller_carlist($db,$msg = null){

	$status = CAR_STATUS_NEW;
	if(!empty($_GET['status'])){
		$status = $_GET['status'];
		$status = sprintf ('%d',$status);
	}

	$db->where('status',$status);
	$cars = $db->get('cars');
	
	$view='CarList';
	include APP_REAL_PATH."/view/view.$view.php";	

}