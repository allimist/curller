<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Cars List</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>

  <body>
    <h1>Car List</h1>

    <?php if(!empty($msg)) echo '<p class="msg">'.$msg.'</p>'; ?>
    
    <p>Car List (You can buy car or sort list by status).</p>

    <a href="?action=carlist&status=<?php print CAR_STATUS_NEW ?>">New Cars</a>
    <a href="?action=carlist&status=<?php print CAR_STATUS_SOLD ?>">Sold Cars</a>

    <?php
      $cars_html = '';
      foreach ($cars as $car) {

        $buy_link_html = '-';
        if ($car['type']==CAR_TYPE_CONVERTIBLE) $type = 'convertible';
        if ($car['type']==CAR_TYPE_MINI) $type = 'mini';
        if ($car['status']==CAR_STATUS_NEW){
          $status = 'New';
          $buy_link_html = '<a href="?action=carbuy&id='.$car['id'].'">Buy</a>';
        } 
        if ($car['status']==CAR_STATUS_SOLD) $status = 'Sold';

        $cars_html.= '<tr><td>'.$car['manufacturer'].'</td><td>'.$car['model'].'</td><td>'.$type.'</td><td>'.$status.'<td>'.$buy_link_html.'</td></tr>';
      }
    ?>
    
    <table border="1">
      <tr>
        <th>Manufacturer</th>
        <th>Model</th>
        <th>Type</th>
        <th>Status</th>
        <th>Buy</th>
      </tr>

      <?php print $cars_html; ?>
    </table>


    <p>
      <a href="?action=menu">Back</a>
    </p>
    

  </body>
</html>