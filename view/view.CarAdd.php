<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Cars List</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script>

      window.onload = function ()
      {
        //add controller error messages if exist (show server validation on client side)
        var error_array = null;
        error_array = JSON.parse('<?php print json_encode($err) ?>');
        for (index in error_array) {
          
          var input_field = document.getElementById(index);
          //turn html5 input error manually (server validation)
          input_field.validity.customError;
          input_field.setCustomValidity(error_array[index]); 

          //remove error in key up
          input_field.addEventListener('keyup',function(){
              this.setCustomValidity('');
          },true);

        }
      }
    </script>
  </head>

  <body>
    <h1>Insert Car</h1>

    <?php if(!empty($msg)) echo '<p class="msg">'.$msg.'</p>'; ?>
    
    <p>Please complete all required fields marked with a <span class="star">*</span>.</p>
    <form name="form1" method="post" action="?action=caradd" id="form">
      <p>
        <label for="manufacturer">Manufacturer <span class="star">*</span></label>
        <input style="" required="" name="manufacturer" id="manufacturer" type="text" value="<?php print (isset($_POST['manufacturer'])) ? $_POST['manufacturer'] : '' ; ?>" >
      </p>
      <p>
        <label for="model">Model <span class="star">*</span></label>
        <input required="" name="model" id="model" type="text"  value="<?php print (isset($_POST['model'])) ? $_POST['model'] : '' ; ?>">
      </p>

      <fieldset class="inline">
        <legend>Type <span class="star">*</span></legend>
        <input required="" name="type" value="<?php echo CAR_TYPE_CONVERTIBLE ?>" id="convertible" type="radio">
        <label for="convertible" class="inline">Convertible</label>
        <input required="" name="type" value="<?php echo CAR_TYPE_MINI ?>" id="mini" type="radio">
        <label for="mini" class="inline">Mini</label>
      </fieldset>

      <p>
        <input required="" name="submit" id="submit" value="Insert" type="submit">
        <a href="?action=menu">Back</a>
      </p>
    </form>

  </body>
</html>